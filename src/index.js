import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import CommentList from './components/CommentList';

const store = configureStore();

render(
  <Provider store={store}>
    <CommentList />
  </Provider>,
  document.getElementById('app')
);

