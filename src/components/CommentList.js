import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchComments } from '../actions/comments';

class CommentList extends Component {
  componentDidMount() {
    this.props.fetchData('https://594ecc215fbb1a00117871a4.mockapi.io/comments');
  }

  render() {
    if (this.props.hasError) {
      return <p> error </p>;
    }
    if (this.props.isLoading) {
      return <p> Loading...</p>;
    }

    return (
      <ul>
        {this.props.comments.map((item) => (
          <li key={item.id}>
            {item.comment}
          </li>
        ))}
      </ul>
    );
  }
}

CommentList.propTypes = {
  fetchData: PropTypes.func.isRequired,
  comments: PropTypes.array.isRequired,
  hasError: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  comments: state.comments,
  hasError: state.getCommentsError,
  isLoading: state.loadComments
});

const mapDispatchToProps= dispatch => ({
  fetchData: (url) => dispatch(fetchComments(url))
});

export default connect(mapStateToProps, mapDispatchToProps)(CommentList);

